import express from "express";
export const router = express.Router();

router.get("/", (req, res) => {
  res.render("index", { titulo: "Practica Examen 02 [Ruanet Ozuna]" });
});

// Parte de Gradoos

router.get("/grados", (req, res) => {
    const params = {
      titulo: "Practica Examen 02 [Ruanet Ozuna]",
      txtCantidad: req.query.txtCantidad,
      slctGrados: req.query.slctGrados,
      gradosconvertidos: ""
    };

    res.render("grados", params);
});

router.post("/grados", (req, res) => {
    const { txtCantidad, grados } = req.body;

    const cantidad = parseFloat(txtCantidad);

    let gradosConvertidos = 0.0;
    if (grados === "celsius-farenheit") {
        gradosConvertidos = (cantidad * 9/5) + 32;
    } else if (grados === "farenheit-celsius") {
        gradosConvertidos = (cantidad - 32) * (5/9);
    }

    res.render("grados", {
        titulo: "Practica Examen 02 [Ruanet Ozuna]",
        txtCantidad: cantidad,
        slctGrados: grados,
        gradosconvertidos: gradosConvertidos.toFixed(2)
    });
});

// Parte de viajes

router.get("/viajes", (req, res) => {
    const params = {
      titulo: "Practica Examen 02 [Ruanet Ozuna]",
      slctTipo: req.query.slctTipo,
      txtPrecio: req.query.txtPrecio,
      subtotal: "",
      impuesto: "",
      total: ""
    };

    res.render("viajes", params);
});

router.post("/viajes", (req, res) => {
    const { txtPrecio, tipo } = req.body;

    const subtotal = parseFloat(txtPrecio);
    let impuesto, total;

    if (tipo === "sencillo") {
        impuesto = subtotal * 0.16;
        total = subtotal + impuesto;
    } else if (tipo === "doble") {
        const nuevoprecio = subtotal * 1.8;
        impuesto = nuevoprecio * 0.16;
        total = nuevoprecio + impuesto;
    }

    res.render("viajes", {
        titulo: "Practica Examen 02 [Ruanet Ozuna]",
        slctTipo: tipo,
        txtPrecio: subtotal,
        impuesto: impuesto.toFixed(2),
        total: total.toFixed(2),
        subtotal: subtotal.toFixed(2)
    });
});



export default { router };

  